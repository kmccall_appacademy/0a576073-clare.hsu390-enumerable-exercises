require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? do |el|
    el.include?(substring)
  end
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  new_str = []
  string.chars do |ch|
    next if ch == " "
    new_str << ch if string.count(ch) > 1
  end
  new_str.uniq.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.delete!("!.;:?-")
  sorted = string.split.sort_by {|el| el.length}
  sorted[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = ("a".."z").to_a
  new_arr = []
  alphabet.each do |ch|
    new_arr << ch unless string.include?(ch)
  end
  new_arr
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  years = (first_yr..last_yr).to_a
  new_arr = [ ]
  years.each do |el|
    new_arr << el if not_repeat_year?(el)
  end
  new_arr
end

def not_repeat_year?(year)
  year.to_s.chars.uniq == year.to_s.chars
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  new_arr = []
  songs.uniq.each do |song|
    new_arr << song if no_repeats?(song, songs)
  end
  new_arr
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |el, idx|
    if song_name == el
      return false if el == songs[idx+1]
    end
  end
  true
end
# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  return " " unless string.include?("c")
  string.delete!(".;!?,-")
  c_words = string.split.select {|el| el.include?("c")}
  ordered = c_words.sort_by {|word| c_distance(word)}
  ordered.sort_by {|idx| string[idx]}.first

end

def c_distance(word)
  word.reverse.index("c")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  new_arr = []
  starting_index = nil
  arr.each_with_index do |el, idx|
    if el == arr[idx+1]
    starting_index = idx unless starting_index
    elsif starting_index
      new_arr << [starting_index, idx]
      starting_index = nil
    end
  end
  new_arr
end
